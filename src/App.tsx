import React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home/";
import LoginPage from "./pages/LoginPage";
import SignUpPage from "./pages/SignUpPage";
import TablePage from "./pages/TablePage";
import { RootStore, useTypedSelector } from "./store/Store";
import Modal from "./components/Modal";


const App: React.FC = () => {
  const modal = useTypedSelector((state: RootStore) => state.modal.isOpen);
  const modalId = useTypedSelector((state: RootStore) => state.modal.data.modalId);
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/signup" element={<SignUpPage />} />
        <Route path="/table" element={<TablePage />} />
        <Route path="*" element={<h1>Not Found</h1>} />
      </Routes>
      {modal && <Modal id={modalId}/>}
    </>
  );
};
export default App;
