import React, {MouseEventHandler} from 'react'
import { buttonConfig } from './buttonConfig';

interface IButton {
    id: string;
    onClick?: MouseEventHandler<HTMLButtonElement>
    children?: React.ReactNode;
  }
const Button: React.FC<IButton> = ({id, onClick, children}) => {
    const btn = buttonConfig[id]
  return (
    <button className={`${btn.className}`} type={btn.type} onClick={onClick}>{btn.text}{children}</button>
  )
}

export default Button