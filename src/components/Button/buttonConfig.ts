import React from "react";

type ButtonType = "button" | "submit" | "reset";
export const buttonConfig: Record<
  string,
  { id?: string; 
    children?:React.ReactNode;
    text: string | React.ReactNode; 
    className: string; 
    type: ButtonType }
> = {
  login: {
    id: "LogIn",
    text: "Log In",
    className: "text-white bg-sky-600 rounded border-none py-2 px-5",
    type: "submit",
  },
  logout: {
    id: "Log Out",
    text: "Log Out",
    className: "text-white bg-sky-600 rounded border-none py-2 px-5",
    type: "submit",
  },
  closeModal: {
    id: "close",
    type: "button",
    className:
      "bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none",
    text: React.createElement("span", {
      className: "w-4 h-4 text-black",
      text: "x",
    }),
  },
  cancel: {
    id: "cancel",
    type: "button",
    text: "Cancel",
    className: "border-none p-2 bg-red-500 rounded-md",
  },
  createUserModal: {
    id: "add",
    text: "add user",
    className: "text-white bg-sky-600 rounded border-none py-2 px-5 my-5",
    type: "button",
  },
  createUser: {
    id: "create-user",
    text: "Create",
    type: "submit",
    className: "p-2 border-none bg-sky-500 rounded-md",
  },
  editUser: {
    id: "edit-user",
    text: "Edit",
    type: "submit",
    className: "p-2 border-none bg-sky-500 rounded-md",
  },
  edit: {
    id: "edit-user",
    text: "",
    type: "button",
    className: "p-2 border-none bg-sky-500 rounded-md",
  },
  delete: {
    id: "delete-user",
    text: "",
    type: "button",
    className: "p-2 border-none bg-sky-500 rounded-md",
  },
};
