import React from "react";
import Button from "../Button";
import { Formik, Form, Field, FormikHelpers, FormikErrors } from "formik";
import * as Yup from "yup";
import {
  EditType,
} from "../../store/action/tableData/tableDataActionTypes";
import { ToggleModal } from "../../store/action/Modal/ModalAction";
import {
  RootStore,
  useTypedDispatch,
  useTypedSelector,
} from "../../store/Store";
import { createTableData, updateTableData } from "../../store/action/tableData/tableDataAction";
interface IFormTable {
  id?: number;
}
//create form
const FormTable: React.FC<IFormTable> = ({ id }) => {
  //variables
  const dispatch = useTypedDispatch();
  const tableData = useTypedSelector(
    (state: RootStore) => state.tableData.tableData
  );
  const selectedData = id && tableData && tableData[id]; // Ensure tableData is not null
  const {
    name: editName,
    birthday_date: editDates,
    email: editEmail,
    phone_number,
    address: editAdress,
  } = selectedData || {};
  const initialValues: EditType = {
    name: editName || "",
    birthday_date: editDates || "",
    email: editEmail || "",
    phone_number: phone_number || "",
    address: editAdress || "",
  };

  const handleSubmit = async (
    values: EditType,
    actions: FormikHelpers<EditType>
  ) => {
    try {
      const UserSchema = Yup.object().shape({
        name: Yup.string()
          .min(1, "Min length name 1 symbol")
          .max(255, "Max length name 255 symbols")
          .required("name is required"),

        birthday_date: Yup.string().required("birthday date is required"),

        email: Yup.string()
          .min(1, "Min length email 1 symbol")
          .max(254, "Max length email 254 symbols")
          .email("should contains @")
          .required("email is required"),

        phone_number: Yup.string()
          .min(1, "Min length phone number 1 symbol")
          .max(20, "Max length phone number 20 symbols")
          .required("phone number is required"),

        address: Yup.string().min(1, "Min length password 1 symbol"),
      });
      await UserSchema.validate(values, { abortEarly: false });
      if (!id) {
        dispatch(createTableData(values))
      }
      if (id) {
        dispatch(updateTableData({...values, id}))
      }
      dispatch(ToggleModal({ currentId: null, modalId: null }));

    } catch (errors) {
      if (Yup.ValidationError.isError(errors)) {
        const formErrors: FormikErrors<any> = {};
        errors.inner.forEach((error) => {
          if (typeof error.path === "string") {
            formErrors[error.path] = error.message;
          }
        });
        actions.setErrors(formErrors);
      }
    }
  };

  //render
  return (
    <Formik initialValues={initialValues} onSubmit={handleSubmit}>
      {({ errors, touched }) => (
        <Form className="flex flex-col gap-4">
          <div className="flex flex-col gap-2 mb-2 relative">
            <label htmlFor="name">Name</label>
            <Field
              required
              id="name"
              name="name"
              placeholder="name"
              className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
            />
            {errors.name && touched.name && (
              <div className="text-red-500 absolute top-20">{errors.name}</div>
            )}
          </div>
          <div className="flex flex-col gap-1 mb-5 relative">
            <label htmlFor="password">Email</label>
            <Field
              required
              id="email"
              name="email"
              placeholder="email"
              className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
            />
            <div className="text-red-500 absolute top-20">{errors.email}</div>
          </div>
          <div className="flex flex-col gap-1 mb-5 relative">
            <label htmlFor="birthday_date">birthday date</label>
            <Field
              required
              type="date"
              id="birthday_date"
              name="birthday_date"
              placeholder="birthday_date"
              className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
            />
            <div className="text-red-500 absolute top-20">
              {errors.birthday_date}
            </div>
          </div>
          <div className="flex flex-col gap-1 mb-5 relative">
            <label htmlFor="phone_number">phone number</label>
            <Field
              required
              id="phone_number"
              name="phone_number"
              placeholder="phone_number"
              className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
            />
            <div className="text-red-500 absolute top-20">
              {errors.phone_number}
            </div>
          </div>
          <div className="flex flex-col gap-1 mb-5 relative">
            <label htmlFor="address">address</label>
            <Field
              id="address"
              name="address"
              placeholder="address"
              className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
            />
            <div className="text-red-500 absolute top-20">{errors.address}</div>
          </div>
          {id && <Button id="editUser" />}
          {!id && <Button id="createUser" />}
          <Button
            id="cancel"
            onClick={() =>
              dispatch(ToggleModal({ currentId: null, modalId: null }))
            }
          />
        </Form>
      )}
    </Formik>
  );
};

export default FormTable;
