import React from 'react'
import CircularProgress from '@mui/material/CircularProgress';

const Loader: React.FC = () => {
    return (
        <CircularProgress disableShrink />
  )
}

export default Loader