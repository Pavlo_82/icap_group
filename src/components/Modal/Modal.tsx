import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Button from '../Button'
import { configModals } from './configModal'
import { ToggleModal } from '../../store/action/Modal/ModalAction'
import { RootStore } from '../../store/Store'
import FormTable from '../Form'


//interface modal
interface IModal {
  id: string | null | undefined
}

//create Modal
const Modal: React.FC<IModal> = ({ id }) => {
  //variables
  let title: string | undefined;
  let text: string | undefined;
  if (id) {
    ({ title, text } = configModals[id])
  }
  const dispatch = useDispatch()

  //useSelector
  const currentId = useSelector((state: RootStore) => state.modal.data?.currentId)
  const tableData = useSelector((state: RootStore) => state.tableData.tableData)
  const currentTableData = (currentId&&tableData)?tableData[currentId]:{}
  //render
  return (
    <>
      <div
        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
        onClick={(e) => { dispatch(ToggleModal({ currentId: null, modalId: null })) }}>
        <div className="relative w-auto my-6 mx-auto max-w-3xl ax-w-[500px]" onClick={e => e.stopPropagation()}>
          {/*content*/}
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            {/*header*/}
            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
              <h3 className="text-3xl font-semibold">
                {title}
              </h3>
              <Button
                id="closeModal"
                onClick={() => {dispatch(ToggleModal({ currentId: null, modalId: null })) }}
              />
            </div>
            <div className="relative p-6 flex-auto">
              <p>{text}</p>
              {id === "createModal" && <FormTable />}
              {id === "editModal"  && <FormTable {...currentTableData} />}
              {/* {id === "deleteModal" && (
                <>
                  <Button id="deleteNotes" onClick={() => {
                    if (currentId) {                  
                      dispatch(deleteNote(currentId))
                    }
                    dispatch(ToggleModal({ currentId: null, modalId: null }))

                  }} />
                  <Button id="cancel" onClick={() => dispatch(ToggleModal({ currentId: null, modalId: null }))} />
                </>
              )} */}
            </div>
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  )
}

export default Modal