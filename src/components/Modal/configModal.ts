//interface
interface IConfigModals {
  title: string;
  text: string;
}

//config for modals
export const configModals:  { [key: string]: IConfigModals } = {
  editModal: {
    title: "Edit User",
    text: "",
  },
  deleteModal: {
    title: "Delete User",
    text: "Are you sure, you want delete?",
  },
  createModal: {
    title: "Create User",
    text: "",
  },
};
