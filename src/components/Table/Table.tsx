import React, { useState } from 'react'
import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import AddIcon from '@mui/icons-material/Add';
import { TableDataType } from '../../store/action/tableData/tableDataActionTypes';
import { RootStore, useTypedDispatch, useTypedSelector } from '../../store/Store';
import { deleteTableData, editTableData, getTableData, paramsAction } from '../../store/action/tableData/tableDataAction';
import Button from '../Button';
import { ToggleModal } from '../../store/action/Modal/ModalAction';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

const TableData: React.FC = () => {
  const dispatch = useTypedDispatch()
  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 40 },
    {
      field: 'name',
      headerName: 'Name',
      width: 180,
      editable: true,
    },
    {
      field: 'birthday_date',
      headerName: 'Birthday Date',
      width: 150,
      editable: true,
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 180,
      editable: true,
    },
    {
      field: 'phone_number',
      headerName: 'Phone Number',
      width: 180,
      editable: true,
    },
    {
      field: 'address',
      headerName: 'Address',
      width: 250,
      editable: true,
    },
    {
      field: 'action',
      headerName: 'Action',
      width: 120,
      editable: false,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', justifyContent: 'center', gap: '10px' }}>
            <Button
              id="edit"
              onClick={() => {
                dispatch(ToggleModal({currentId: params.row.id, modalId: 'editModal'}))
              }}
            >
              <EditIcon/>
            </Button>
            <Button
              id="delete"
              onClick={() => {
                dispatch(deleteTableData(params.row.id))
              }}
            >
              <DeleteIcon/>
            </Button>
          </Box>
        )
      }
    }
  ];
  const limit = useTypedSelector(
    (state: RootStore) => state.tableData.params.limit
  );
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  
  const tableData = useTypedSelector((state: RootStore) =>
    Object.values(state.tableData.tableData || {}) as TableDataType[]
  );
  const rowCount = useTypedSelector(
    (state: RootStore) => state.tableData.params.count
  );
  const isLoading = useTypedSelector(
    (state: RootStore) => state.tableData.loading
  );

  const offset = useTypedSelector(
    (state: RootStore) => state.tableData.params.offset
  );

  const handlePagination = (page: number) => {
    const newoffset = (offset && limit) ? limit * (page) : 10
    dispatch(paramsAction({
      limit: limit,
      offset: newoffset,
      count: rowCount
    }))
    dispatch(getTableData())
    setPage(page)
  }

  const handleCellEditStop = (data: any) => {
    const tableData = data.row
    tableData[data.field] = data.value
    const date = new Date(tableData.birthday_date)
    tableData.birthday_date = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
    dispatch(editTableData(tableData));
  };

  const handlePageSize = (limit: number) => {
    dispatch(paramsAction({
      limit: limit,
      offset: limit * page,
      count: rowCount
    }))
    dispatch(getTableData())
    setPageSize(limit)
  }

  const handleOpenModal = () => {
    dispatch(ToggleModal({  modalId: 'createModal' }))
  }

  return (
    <div style={{ height: '90vh', width: '100%' }}>
      <Button id="createUserModal" onClick={handleOpenModal}><AddIcon/></Button>
      <Box style={{ height: '100%', width: '100%' }}>
        <DataGrid
          style={{ height: '100%', width: '100%' }}
          onCellEditCommit={handleCellEditStop}
          rows={tableData}
          columns={columns}
          rowCount={rowCount}
          loading={isLoading}
          pagination
          page={page}
          pageSize={pageSize}
          paginationMode="server"
          onPageChange={handlePagination}
          onPageSizeChange={handlePageSize}
          rowsPerPageOptions={[5, 10, 20]}
        />
      </Box>
    </div>
  )
}

export default TableData
