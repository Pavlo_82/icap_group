import React, { useState } from "react";
import {
  Formik,
  Form,
  Field,
  FormikHelpers,
  FormikErrors,
} from "formik";
import * as Yup from "yup";
import {
  RootStore,
  useTypedDispatch,
  useTypedSelector,
} from "../../store/Store";
import { useNavigate, NavLink } from "react-router-dom";
import {
  UserType,
} from "../../store/action/session/sessionActionTypes";
import { logIn } from "../../store/action/session/sessionAction";
import Button from "../../components/Button";
import { AiFillEyeInvisible, AiFillEye } from "react-icons/ai";

const LoginPage: React.FC = () => {
  const dispatch = useTypedDispatch();
  const navigate = useNavigate();
  const error = useTypedSelector((state: RootStore) => state.session.error);

  const [isPassShown, setPassShown] = useState(false);
  const initialValues: UserType = {
    username: "",
    password: "",
  };
  const handleSubmit = async (
    values: UserType,
    actions: FormikHelpers<UserType>
  ) => {
    try {
      const UserSchema = Yup.object().shape({
        username: Yup.string()
          .min(1, "Min length username 1 symbol")
          .max(150, "Max length username 150 symbols")
          .required("username is required"),

        password: Yup.string()
          .min(1, "Min length password 1 symbol")
          .max(128, "Max length password 128 symbols")
          .required("Password is required"),
      });
      await UserSchema.validate(values, { abortEarly: false });

      const user: any = await dispatch(logIn(values));
      if (user) {
        actions.setSubmitting(false);
        navigate(`/table`);
      }
    } catch (errors) {
      if (Yup.ValidationError.isError(errors)) {
        const formErrors: FormikErrors<any> = {};
        errors.inner.forEach((error) => {
          if (typeof error.path === "string") {
            formErrors[error.path] = error.message;
          }
        });
        actions.setErrors(formErrors);
      }
    }
  };
  return (
    <div className="max-w-[500px] mx-auto my-[20px] flex flex-col gap-10">
      <h2 className="mb-6 text-2xl text-center capitalize">Log In</h2>
      <Formik initialValues={initialValues} onSubmit={handleSubmit}>
        {({ errors, touched }) => (
          <Form className="flex flex-col gap-4">

            <div className="flex flex-col gap-3 mb-10 relative">
              <label htmlFor="username">Username</label>

              <Field
                required
                id="username"
                name="username"
                placeholder="Username"
                className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
              />
              {errors.username && touched.username && (
                <div className="text-red-500 absolute top-20">{errors.username}</div>
              )}
               {error && (
                <div className="text-red-500 absolute top-20">{error.error}</div>
              )}
            </div>
            <div className="flex flex-col gap-3 mb-10 relative">
              <label htmlFor="password">Password</label>
              <Field
                required
                type={isPassShown ? "text" : "password"}
                id="password"
                name="password"
                placeholder="Password"
                className="text-sky-500 border-sky-500 rounded-md p-2 border-solid border-2 pr-10"
              />
              {!isPassShown && (
                <AiFillEyeInvisible
                  className="cursor-pointer text-sky-500 text-lg absolute top-[63%] right-4"
                  onClick={() => setPassShown(true)}
                />
              )}
              {isPassShown && (
                <AiFillEye
                  className="cursor-pointer text-sky-500 text-lg absolute top-[63%] right-4"
                  onClick={() => setPassShown(false)}
                />
              )}
              {errors.password && touched.password && (
                <div className="text-red-500 absolute top-20">{errors.password}</div>
              )}
              {error && (
                <div className="text-red-500 absolute top-20">{error.error}</div>
              )}
            </div>
            <Button id="login" />
          </Form>
        )}
      </Formik>
      <p className="mt-2 text-base">
        If you do not have account{" "}
        <NavLink className="text-sky-500 text-xl" to="/signup">
          Sign Up
        </NavLink>
      </p>
    </div>
  );
};

export default LoginPage;
