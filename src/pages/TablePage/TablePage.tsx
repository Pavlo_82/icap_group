import React, { useEffect } from "react";
import {
  useTypedDispatch,
  useTypedSelector,
  RootStore,
} from "../../store/Store";
import TableData from "../../components/Table";
import { getTableData } from "../../store/action/tableData/tableDataAction";
import Loader from "../../components/Loader";
import { UserType } from "../../store/action/session/sessionActionTypes";
import { useNavigate } from "react-router-dom";

const TablePage: React.FC = () => {
  const navigate = useNavigate();
  const user: UserType | null = useTypedSelector(
    (state: RootStore) => state.session.user
  );

  const dispatch = useTypedDispatch();
  const isLoading = useTypedSelector(
    (state: RootStore) => state.tableData.loading
  );

  useEffect(() => {
    dispatch(getTableData());
  }, [dispatch]);
  if (!user) {
    navigate("/login");
  }  
  return (
    <main>
      <div className="max-w-[1200px] mx-auto my-[20px]">
        {isLoading && <Loader />}
        {!isLoading && <TableData />}
      </div>
    </main>
  );
};

export default TablePage;
