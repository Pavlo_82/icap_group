import {
    ModalDispatchTypes,
    TOGGLE_MODAL,
    IModal,
  } from "./ModalActionTypes";
  
  
  export const ToggleModal = (data: IModal): ModalDispatchTypes => ({
    type: TOGGLE_MODAL,
    payload: data,
  });