//types
export const TOGGLE_MODAL = "TOGGLE_MODAL";

//interface
export interface IModal {
  currentId?: number | null;
  modalId?: string | null;
}

export interface ModalOpen {
  type: typeof TOGGLE_MODAL;
  payload: IModal
}

export type ModalDispatchTypes = ModalOpen