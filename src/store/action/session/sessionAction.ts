import { Dispatch } from "redux";
import {
  USER_FAIL,
  USER_SUCCESS,
  UserType,
  UserDispatchTypes,
} from "./sessionActionTypes";
import { TypedThunkAction } from "../../Store";
import CustomError from "../../../Error/CustomError";

export const logIn =
  (user: UserType): TypedThunkAction =>
    async (dispatch: Dispatch<UserDispatchTypes>) => {
      try {
        const res = await fetch('https://technical-task-api.icapgroupgmbh.com/api/login/', {
          method: 'POST',
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(user)
        })

        if (!res.ok) {
          return await new CustomError('Invalid credentials');
        }
          dispatch({
            type: USER_SUCCESS,
            payload: user,
          });
          return res
      } catch (e: any) {
          dispatch({
            type: USER_FAIL,
            payload: { error: 'Invalid credentials - your username or password is incorect' },
          });
      }
    };

