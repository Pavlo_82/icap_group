//types
export const USER_FAIL = "USER_FAIL";
export const USER_SUCCESS = "USER_SUCCESS";

//interfaces
export type UserType = {
  id?: number;
  username: string;
  password?: string;
};

export interface ErrorUserType {
  error: string;
}

export interface UserFail {
  type: typeof USER_FAIL;
  payload: ErrorUserType;
}

export interface UserSuccess {
  type: typeof USER_SUCCESS;
  payload: UserType;
}

//All types actions
export type UserDispatchTypes = UserFail | UserSuccess;
