import { Dispatch } from "redux";
import {
  TABLE_DATA_FAIL,
  TABLE_DATA_LOADING,
  TABLE_CREATE,
  TABLE_DELETE,
  TABLE_DATA_SUCCESS,
  PARAMS_SUCCESS,
  TABLE_EDIT,
  TableDataType,
  TableDataDispatchTypes,
  Params,
  EditType,
} from "./tableDataActionTypes";
import { TypedThunkAction } from "../../Store";

const baseUrl = 'https://technical-task-api.icapgroupgmbh.com/api/table'

export const paramsAction = (params: Params) => ({
  type: PARAMS_SUCCESS as typeof PARAMS_SUCCESS,
  payload: params,
});

export const getTableData =
  (): TypedThunkAction =>
  async (dispatch: Dispatch<TableDataDispatchTypes>, state:any) => {
    // dispatch({
    //   type: TABLE_DATA_LOADING,
    // });
    try {
      const globalState = state();
      const limit = globalState.tableData.params.limit;
      const offset = globalState.tableData.params.offset;
      const offsetParam = offset ? `&offset=${offset}` : "";

      const res = await fetch(
        `${baseUrl}/?limit=${limit}${offsetParam}`
      );
      if (!res.ok) {
        throw new Error("something went wrong");
      }
      const data = await res.json();
      dispatch({
        type: TABLE_DATA_SUCCESS,
        payload: data.results,
      });
      dispatch(
        paramsAction({
          limit: data.limit || limit,
          offset: data.offset || offset,
          count: data.count,
        })
      );
      return res;
    } catch (e: any) {
      dispatch({
        type: TABLE_DATA_FAIL,
        payload: { error: e.message },
      });
    }
  };


  export const editTableData =
  (tableData:TableDataType): TypedThunkAction =>
  async (dispatch: Dispatch<TableDataDispatchTypes>) => {
    // dispatch({
    //   type: TABLE_DATA_LOADING,
    // });
    try {
      const res = await fetch(
        `${baseUrl}/${tableData.id}/`, {
          headers: { 'Content-Type': 'application/json'},
          method: 'PATCH',
          body: JSON.stringify(tableData)
        }
      );
      if (!res.ok) {
        throw new Error("something went wrong");
      }
      const data = await res.json();
      dispatch({
        type: TABLE_EDIT,
        payload: data.results,
      });
      return res;
    } catch (e: any) {
      dispatch({
        type: TABLE_DATA_FAIL,
        payload: { error: e.message },
      });
    }
  };

  export const updateTableData =
  (tableData:TableDataType): TypedThunkAction =>
  async (dispatch: Dispatch<TableDataDispatchTypes>) => {
    // dispatch({
    //   type: TABLE_DATA_LOADING,
    // });
    try {
      const res = await fetch(
        `${baseUrl}/${tableData.id}/`, {
          headers: { 'Content-Type': 'application/json'},
          method: 'PUT',
          body: JSON.stringify(tableData)
        }
      );
      if (!res.ok) {
        throw new Error("something went wrong");
      }
      const data = await res.json();
      dispatch({
        type: TABLE_EDIT,
        payload: data.results,
      });
      return res;
    } catch (e: any) {
      dispatch({
        type: TABLE_DATA_FAIL,
        payload: { error: e.message },
      });
    }
  };

  export const createTableData =
  (tableData:EditType): TypedThunkAction =>
  async (dispatch: Dispatch<TableDataDispatchTypes>) => {
    // dispatch({
    //   type: TABLE_DATA_LOADING,
    // });
    try {
      const res = await fetch(
        `${baseUrl}/`, {
          headers: { 'Content-Type': 'application/json'},
          method: 'POST',
          body: JSON.stringify(tableData)
        }
      );
      if (!res.ok) {
        const data = await res.json();
        if(data.email){
          throw new Error('email is exist');
        }
        if(data.name){
          throw new Error('name is exist');
        }
        
      }
      const data = await res.json();
      dispatch({
        type: TABLE_CREATE,
        payload: data.results,
      });
      return res;
    } catch (e: any) {     
      dispatch({
        type: TABLE_DATA_FAIL,
        payload: { error: e.message },
      });
    }
  };

  export const deleteTableData =
  (id:number): TypedThunkAction =>
  async (dispatch: Dispatch<TableDataDispatchTypes>) => {
    // dispatch({
    //   type: TABLE_DATA_LOADING,
    // });
    try {
      const res = await fetch(
        `${baseUrl}/${id}/`, {
          headers: { 'Content-Type': 'application/json'},
          method: 'DELETE',
        }
      );
      if (!res.ok) {
        throw new Error("something went wrong");
      }
      dispatch({
        type: TABLE_DELETE,
        payload: id,
      });
      // return res;
    } catch (e: any) {
      dispatch({
        type: TABLE_DATA_FAIL,
        payload: { error: e.message },
      });
    }
  };