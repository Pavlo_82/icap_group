//types
export const TABLE_DATA_FAIL = "TABLE_DATA_FAIL";
export const TABLE_DATA_SUCCESS = "TABLE_DATA_SUCCESS";
export const PARAMS_SUCCESS = "PARAMS_SUCCESS";
export const TABLE_DATA_LOADING = 'TABLE_DATA_LOADING'
export const TABLE_EDIT = 'TABLE_EDIT'
export const TABLE_CREATE = 'TABLE_CREATE'
export const TABLE_DELETE = 'TABLE_DELETE'

//interfaces
export type TableDataType = {
  id: number;
  name: string;
  birthday_date: string;
  email: string;
  phone_number: string;
  address: string;
};

export type EditType = {
  name: string;
  birthday_date: string;
  email: string;
  phone_number: string;
  address: string;
};

export type Params ={
  limit?: number;
  offset?: number;
  search?: string;
  count?: number;
}

export interface ErrorTableDataType {
  error: string;
}

export interface TableDataFail {
  type: typeof TABLE_DATA_FAIL;
  payload: ErrorTableDataType;
}

export interface TableDataLoading {
  type: typeof TABLE_DATA_LOADING;
}

export interface ParamsSuccess {
  type: typeof PARAMS_SUCCESS;
  payload: Params;
}

export interface TableEditSuccess {
  type: typeof TABLE_EDIT;
  payload: TableDataType;
}

export interface TableDeleteSuccess {
  type: typeof TABLE_DELETE;
  payload: number;
}

export interface TableCreateSuccess {
  type: typeof TABLE_CREATE;
  payload: TableDataType;
}

export interface TableDataSuccess {
  type: typeof TABLE_DATA_SUCCESS;
  payload: Array<TableDataType>;
}

//All types actions
export type TableDataDispatchTypes =
  | TableDataFail
  | TableDataSuccess
  | ParamsSuccess
  | TableDataLoading
  | TableEditSuccess
  | TableCreateSuccess
  |TableDeleteSuccess;
