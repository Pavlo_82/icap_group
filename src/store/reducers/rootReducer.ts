import { combineReducers } from "redux";
import sessionReducer from "./sessionReducer"
import tableDataReducer from "./tableDataReducer";
import ModalReducer from "./modalReducer";

//combine reducers
const rootReducer = combineReducers({
    session: sessionReducer,
    tableData: tableDataReducer,
    modal: ModalReducer,
});

export default rootReducer;