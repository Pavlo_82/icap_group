import {
  USER_FAIL,
  USER_SUCCESS,
  UserType,
  UserDispatchTypes,
  ErrorUserType,
} from "../action/session/sessionActionTypes";

interface IInitialState {
  loading?: boolean;
  user:  UserType  | null;
  error: ErrorUserType | null;
}

//initial state
const initialState: IInitialState = {
  loading: true,
  user: null,
  error: null,
};

//note reducer
const sessionReducer = (state = initialState, action: UserDispatchTypes): typeof initialState => {
  switch (action.type) {
    case USER_FAIL:
      return { ...state, error: action.payload };
    case USER_SUCCESS:          
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

export default sessionReducer;
