import {
  TABLE_DATA_FAIL,
  TABLE_DATA_SUCCESS,
  PARAMS_SUCCESS,
  TABLE_DELETE,
  TABLE_DATA_LOADING,
  TABLE_EDIT,
  TableDataType,
  TableDataDispatchTypes,
  ErrorTableDataType,
  Params,
} from "../action/tableData/tableDataActionTypes";

interface IInitialState {
  loading?: boolean;
  tableData: { [key: string]: TableDataType } | null;
  error: { [key: string]: ErrorTableDataType } | null;
  params: Params;
}

//initial state
const initialState: IInitialState = {
  loading: true,
  tableData: null,
  error: null,
  params: {
    limit: 10,
    offset: 0,
    count: 1,
  },
};

//note reducer
const tableDataReducer = (
  state = initialState,
  action: TableDataDispatchTypes
): typeof initialState => {
  switch (action.type) {
    case TABLE_DATA_FAIL:
      const error: { [key: string]: ErrorTableDataType } = { ...state.error };
      error[action?.payload.error] = action.payload;
      return { ...state, error };
    case PARAMS_SUCCESS:
      return { ...state, params: action.payload };
    case TABLE_DATA_LOADING:
      return { ...state, loading: true };
    case TABLE_EDIT:
      const updatedState = { ...state, tableData: { ...state.tableData } };
      updatedState.tableData[action.payload.id] = action.payload;
      return { ...updatedState };
    case TABLE_DELETE:
      const deletedState = { ...state };
      if (deletedState.tableData) {
        delete deletedState.tableData[action.payload];
        return deletedState;
      }
      return state;
    case TABLE_DATA_SUCCESS:
      let data = {};
      if (action.payload) {
        data = action.payload.reduce((acc, cur) => {
          acc[cur.id] = cur;
          return acc;
        }, {} as { [key: string]: TableDataType });
      }
      return { ...state, tableData: data, loading: false };
    default:
      return state;
  }
};

export default tableDataReducer;
